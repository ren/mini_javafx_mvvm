package sample;

import java.util.HashSet;
import java.util.List;

/**
 * @author can ren
 * @create $(YEAR)-$(MONTH)-$(DAY)
 */
public class User {

    String name;
    String saying;
    HashSet userList;

    public User() {
    }

    public User(String name) {
        this.name = name;
        userList = new HashSet();
        userList.add("Miki");
    }

    public User(String name, String saying) {
        this.name = name;
        this.saying = saying;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", saying='" + saying + '\'' +
                '}';
    }

    public String checkUserName(String name) {

        if(userList.contains(name)){
            return "user name exited!!";
        }else{
            return "set successfully";
        }
    }
}
