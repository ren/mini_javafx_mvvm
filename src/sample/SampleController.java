package sample;


import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class SampleController {

    @FXML
    public TextField enterUserName;

    @FXML
    public TextField result;

    private SampleViewModel sampleVM;

    public void init(SampleViewModel sampleVM){
        this.sampleVM = sampleVM;
        enterUserName.textProperty().bindBidirectional(sampleVM.userNameProperty());
        result.textProperty().bindBidirectional(sampleVM.checkProperty());
    }

    public void checkUnserName(){
        sampleVM.checkName();
    }


}
