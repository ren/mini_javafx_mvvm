package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author can ren
 * @create $(YEAR)-$(MONTH)-$(DAY)
 */
public class SampleViewModel {


    private StringProperty userName;

    private StringProperty check;

    private User user;


    public SampleViewModel(User user){
        this.user = user;
        userName = new SimpleStringProperty();
        check = new SimpleStringProperty();
    }


    public String getUserName() {
        return userName.get();
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public String getCheck() {
        return check.get();
    }

    public StringProperty checkProperty() {
        return check;
    }


    @Override
    public String toString() {
        return "SampleViewModel{" +
                "userName=" + userName +
                ", user=" + user +
                '}';
    }

    public void checkName() {
        System.out.println("here ist check name function!");

        String name = userName.get();

        String result = user.checkUserName(name);
        check.set(result);
    }
}
